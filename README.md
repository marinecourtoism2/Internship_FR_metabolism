# Internship_FR_metabolism

## Datasets and R source code of manuscript "Do physiological requirements predict the trophic needs of a species? A comparative study in freshwater fish." 

### Glossary 
- Functional response parameters: 
**a**: space clearance rate, 
**h**: handling time,
**a/h**: functional response ratio (FRR)
- Metabolic rates:
**SMR**: standard metabolic rate, 
**RMR**: routine metabolic rate 


### File "FR_checks"

Folder containing the raw data of the FR articles in "txt" format as well as the scripts allowing to compare the obtained FR parameters with the FR parameters listed in the FoRAGE database and in the original items. 

### File "Extraction_Metabolic_Data"

Folder containing the figures of the articles from which the metabolic data have been extracted. The data extraction method is listed in the R script "Extraction_Metabolism_Data.csv".  

### Script "test_RptR.R"

R script detailing repeatability tests of FR parameters and metabolic rates for the 44 freshwater fish species studied. 
Creation of the associated graphs corresponding to the **Figures 16, 17 and 18** in the Appendices section of the report 

### Script "Conversion_Metabolic_Data.R"

The script consists of two parts.
The first part of the script converts the metabolic data into the same temperature as the functional response data using the Q_10 formula. 
The second part of the script allows the creation of the table of crossing of the FR and metabolic data. A crossover table has been created for the data set ("FR_meta_data_all_data.csv"). A second crossover table was created only for the metabolic data published after 1990 ("FR_meta_data_dataafter90.csv"). 

**Functional response dataset** : "FR_Data.csv"
FoRAGE database (Uiterwaal et al, 2018) limited to freshwater fish enriched with data from Fernandez Declerck et al, 2021. 
The dataset corresponds to data frame "FR" in the script. Variables description:

- data_set: unique number assigned to functional response data pairs from the same article
- source: reference of the original article of the data 
- predation_or_parasitism: type of consumer 
- dim: dimension, if dim = 2 the consumer feeds only on the bottom of the aquarium, if dim = 3 the consumer feeds in 3 dimensions 
- habitat: aquatic 
- fresh_or_marine: fish category, here fresh (water fish)
- predator_cellularity
- vert_invert: vertebrate predators 
- major_grouping_1: fish 
- major_grouping_2: order 
- predator_scientific_name
- temp_type: "thermal type" is composed of two categories: "eurytherm" or "stenotherm
- dispersal_type: dispersal capacity classified into two categories "migrant" and "non-migrant"
- location_type: Location type specifies the position of the species in the water column. Three categories are distinguished: species can be pelagic, benthopelagic or demersal.
- species_type: indicates whether the species is "invasive" or "non-invasive"
- nb_countries_introduced: number of countries into which the species has been introduced 
- evidence_of_impact: describes whether "yes" or "no" the species is having an impact in at least one of the countries into which it has been introduced
- predator_type: age of the predator 
- trophic_level_predator 
- prey_cellularity
- vert_invert.1: "vertebrate" or "invertebrate" prey 
- major_grouping_1.1: major grouping of prey
- major_grouping_2.1: order of prey 
- prey: common name of prey 
- prey_scientific_name
- prey_type 
- trophic_level_prey 
- data_type: "mean" or "raw data"
- prey_replaced: indicates whether prey was replaced (Y) or not (N) during the functional response measurement
- hours_starved: hours of fasting required before the functional response measurement
- temp_celsius: temperature measurement of the functional response in degrees Celsius
- pred_per_arena: number of predators per arena 
- X2D_arena_size_cm2_bottom_SA: arena size in cm^2
- X3D_arena_size_cm3: arena size in cm^3
- predator_mass_mg
- prey_mass_mg
- units_of_a
- mean_R2_of_fits: mean R2
- fitted_a_median_of_BS_samples: Value of a
- CI_a_low: low confidence interval for a 
- CI_a_hi: high confidence interval for a 
- fitted_h_day: value of h in days 
- CI_h_low: low confidence interval for a 
- CI_h_hi: high confidence interval for a 

**Metabolism dataset**: "Metabolism_Data.csv"
Metabolic database created by extracting SMR and RMR data from the published literature. The extraction method is detailed in the script "Extraction_Metabolism_Data.R"
The dataset corresponds to data frame "meta" in the script. Variables description:

- data_set: unique number assigned to functional response data pairs from the same article
- data_name: reference of the original article of the data 
- date: year of publication 
- scientific_name 
- super_class
- class
- sub_class
- infra_class
- super_order
- order
- sub_order
- super_family
- family
- sub_family 
- genus 
- species 
- fresh_or_marine: fish category, here fresh (water fish)
- max_length_cm: maximum length in cm 
- common_length_cm
- length_type: TL for Total Length, SL for Standard Length and FL for Fork Length 
- mass_g: mass of fishes in g 
- sexe: "female", "male", "unsexed"
- method: method of measuring metabolic rates 
- trial_duration_h: time of the measurement protocol 
- measuring_duration_min: time only of the measurement phase 
- hours_starved: hours of fasting required before metabolic rate measurement
- temp_celsius: temperature measurement in degrees Celsius
- units_of_oxygen_consumption 
- oxygen_consumption: is equivalent to the metabolic rate 
- salinity 
- activity: "standard", "routine", "active"
- applied_stress: stress applied to individuals during the measurement 
- sub_data : "dataset1", "dataset2", ... : associates a dataset with a population if the article in question studies different populations 
- data_type : "raw_data", "mean_given", "mean_digitized", "ind_digitized" or "fishbase" : describes how the data for each line was obtained
- fish_headcount : number of fish associated with each data item

### Script "phylogeny_freshwater_fish.R"
Script to obtain **Phylogenetic tree** : "fishtree.phy".
Tree shown in **Figure 7** of the report

### Script "Analyses_RF_meta_data.R"

Script in which all the analyses of the crossed dataset have been realized, including the MCMCglmm analyses to take into account the phylogeny of the fishes, as well as the associated graphics.

**Main cross-referenced dataset**: "FR_meta_data_alldata_mean_method.csv" or "FR_meta_data_alldata_weighted_mean_method.csv"
Data table crossing RF and metabolic data whose method of obtaining is detailed in the "Conversion_Metabolic_Data.R" script.
The dataset corresponds to data frame "FR_meta_data" in the script. Variables description:

- species: scientific name of freshwater fish
- temp_type: "thermal type" is composed of two categories: "eurytherm" or "stenotherm
- dispersal_type: dispersal capacity classified into two categories "migrant" and "non-migrant"
- location_type: Location type specifies the position of the species in the water column. Three categories are distinguished: species can be pelagic, benthopelagic or demersal.
- species_type: indicates whether the species is "invasive" or "non-invasive"
- nb_countries_introduced: number of countries into which the species has been introduced 
- evidence_of_impact: describes whether "yes" or "no" the species is having an impact in at least one of the countries into which it has been introduced
- trial_duration_fr_day: functional response measurement time in days
-  dim_a: specifies whether a is calculated in 2 dimensions or in 3 dimensions 
-  units_of_a
-  a: value of a 
-  esm_a: standard deviation from the mean (inter-study uncertainty) for a 
-  h: value of h in days 
-  esm_h: standard deviation from the mean (inter-study uncertainty) for h
-  a_h_ratio: value of functional response ratio (FRR)
-  esm_a_h_ratio: standard deviation from the mean (inter-study uncertainty) for FRR
-  SMR: value of SMR
-  esm_SMR: standard deviation from the mean (inter-study uncertainty) for SMR
-  RMR: value of RMR
-  esm_RMR: standard deviation from the mean (inter-study uncertainty) for RMR
-  temp_celsius: temperature common to FR parameters and metabolic rates in degrees Celsius. 
-  nb_art_meta: number of articles from which metabolic data were extracted
-  nb_art_FR: number of articles from which FR data were extracted
-  unit_a_biomass: unit of a converted in biomass
-  a_biomass: value of a converted in biomass
-  esm_a_biomass : standard deviation from the mean (inter-study uncertainty) for a converted in biomass
-  h_biomass: value of h converted in biomass
-  esm_h_biomass : standard deviation from the mean (inter-study uncertainty) for h converted in biomass
-  a_h_ratio_biomass: value of FRR converted in biomass
-  esm_a_h_ratio_biomass : standard deviation from the mean (inter-study uncertainty) for FRR converted in biomass

**Secondary cross-referenced dataset**: "FR_meta_data_dataafter90.csv"
Data table crossing RF and metabolic data (only published after 1990) whose method of obtaining is detailed in the script "Conversion_Metabolic_Data.R".
The dataset corresponds to data frame "FR_meta_data_90" in the script. Variables description is the same as for 
the main cross-referenced dataset.

### References 

- **Stella F. Uiterwaal et al.** “Data paper : Fo- RAGE (Functional Responses from Around the Globe in all Ecosystems) database : a compila- tion of functional responses for consumers and parasitoids”. In : (26 dec. 2018). Type : article, p. 503334. url : https://www.biorxiv.org/content/10.1101/503334v1
- **Marina Fernandez Declerck**. “Comparison of the ecological and physiological performance in a context of noise pollution of two freshwater fish species, one native and one invasive: the Rhone Apron and the Round Goby. M2 internship report”. In : (2021).



